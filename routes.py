#Grundaufbau und Basic-Funktionen 1zu1 übernommen aus Beispiel aus Unterricht: https://github.com/miguelgrinberg/microblog
# Alle applikationsspezifischen Funktionen sind eigenentwicklung mit Inspiration von: https://github.com/miguelgrinberg/microblog/blob/main/app/auth/routes.py, https://flask.palletsprojects.com/en/1.0.x/, https://flask-login.readthedocs.io/en/latest/, https://flask-restful.readthedocs.io/en/latest/, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker
from flask import render_template, flash, redirect, url_for, request, jsonify, abort
from flask_login import current_user, login_user, logout_user, login_required
from app import app, db
from models import User, Todo, Comment, TrackedTodo
from forms import LoginForm, RegistrationForm, TodoForm, CommentForm
from datetime import datetime

# Startseite Funktion um ToDos sortieren zu können
@app.route('/')
@app.route('/home')
@login_required
def home():
    sort_by = request.args.get('sort', 'due_date')
    if sort_by not in ['title', 'priority', 'department', 'due_date']:
        sort_by = 'due_date'
    if sort_by == 'due_date':
        todos = Todo.query.order_by(Todo.due_date.asc()).all()
    else:
        todos = Todo.query.order_by(getattr(Todo, sort_by).desc()).all()
    return render_template('home.html', todos=todos, sort_by=sort_by)

# Funktion für das Login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Ungültiger Username oder Passwort')
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('home'))
    return render_template('login.html', title='Sign In', form=form)

# Funktion für das logout
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

# Funktion für die Registration
@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Gratulation! Sie sind nun erfolgreich registriert.')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

# Funktion um einen ToDo zu kommentieren
@app.route('/todo/<int:todo_id>', methods=['GET', 'POST'])
@login_required
def todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(body=form.body.data, todo_id=todo.id, user_id=current_user.id)
        db.session.add(comment)
        db.session.commit()
        flash('Ihr Kommentar wurde hinzugefügt.')
        return redirect(url_for('todo', todo_id=todo.id))
    comments = Comment.query.filter_by(todo_id=todo.id).order_by(Comment.timestamp.asc()).all()
    return render_template('todo.html', todo=todo, comments=comments, form=form)

# Funktion um einen ToDo abzuschliessen
@app.route('/complete_todo/<int:todo_id>')
@login_required
def complete_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    todo.completed_at = datetime.utcnow()
    todo.status = "Closed"
    db.session.commit()
    flash('Der ToDo wurde abgeschlossen')
    return redirect(url_for('todo', todo_id=todo.id))

# Funktion um einen ToDo erneut zu öffnen
@app.route('/reopen_todo/<int:todo_id>')
@login_required
def reopen_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    todo.completed_at = None
    todo.status = "Open"
    db.session.commit()
    flash('Der ToDo wurde wiedereröffnet.')
    return redirect(url_for('todo', todo_id=todo.id))

# Funktion um einen neuen ToDo zu erfassen
@app.route('/new_todo', methods=['GET', 'POST'])
@login_required
def new_todo():
    form = TodoForm()
    if form.validate_on_submit():
        todo = Todo(title=form.title.data, description=form.description.data, priority=form.priority.data, department=form.department.data, due_date=form.due_date.data, author=current_user)
        db.session.add(todo)
        db.session.commit()
        flash('Der ToDo wurde hinzugefügt.')
        return redirect(url_for('home'))
    return render_template('new_todo.html', title='New Todo', form=form)

# Funktion um die überwachten ToDos aufzulisten
@app.route('/tracked_todos')
@login_required
def tracked_todos():
    tracked_todos = TrackedTodo.query.filter_by(user_id=current_user.id).all()
    todos = [tracked_todo.todo for tracked_todo in tracked_todos]
    return render_template('tracked_todos.html', todos=todos)

# Funktion um einen ToDo zu überwachen
@app.route('/track_todo/<int:todo_id>')
@login_required
def track_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    tracked_todo = TrackedTodo.query.filter_by(user_id=current_user.id, todo_id=todo.id).first()
    if tracked_todo is None:
        tracked_todo = TrackedTodo(user_id=current_user.id, todo_id=todo.id)
        db.session.add(tracked_todo)
        db.session.commit()
        flash('Du überwachst nun diesen ToDo.')
    else:
        db.session.delete(tracked_todo)
        db.session.commit()
        flash('Du überwachst nun diesen ToDo nicht mehr.')
    return redirect(url_for('todo', todo_id=todo.id))

# Funktion um die Profil-Seite mit ToDos anzeigen zu lassen
@app.route('/profile')
@login_required
def profile():
    my_open_todos_list = my_open_todos()
    return render_template('profile.html', title='Profil', my_open_todos=my_open_todos_list)

# Funktion um die eigenen offenen ToDos anzeigen zu lassen
@app.route('/my_open_todos')
@login_required
def my_open_todos():
    todos = Todo.query.filter_by(status='Open', author=current_user).order_by(Todo.due_date.asc()).all()
    return todos

# Funktion um einen ToDo zu löschen, diese Funktion prüft auch gleich, dass nur der Autor die ToDo löschen kann
@app.route('/delete_todo/<int:todo_id>', methods=['POST'])
@login_required
def delete_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    if todo.author != current_user:
        abort(403) 
    comments = Comment.query.filter_by(todo_id=todo_id).all()
    for comment in comments:
        db.session.delete(comment)
    db.session.delete(todo)
    db.session.commit()
    flash('Ihr Todo-Eintrag wurde entfernt.', 'success')
    return redirect(url_for('profile'))

# Funktion um einen ToDo zu bearbeiten, diese Funktion prüft auch gleich, dass nur der Autor die ToDo bearbeiten kann
@app.route('/edit_todo/<int:todo_id>', methods=['GET', 'POST'])
@login_required
def edit_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    form = TodoForm(obj=todo)
    if todo.author != current_user:
        abort(403)  # Forbidden, der Benutzer hat keine Berechtigung zum Bearbeiten dieses Todo-Eintrags
        form = TodoForm(obj=todo)
    if form.validate_on_submit():
        todo.title = form.title.data
        todo.description = form.description.data
        todo.priority = form.priority.data
        todo.department = form.department.data
        todo.due_date = form.due_date.data
        db.session.commit()
        flash('Ihr Todo-Eintrag wurde aktualisiert.', 'success')
        return redirect(url_for('todo', todo_id=todo.id))
    return render_template('edit_todo.html', title='Edit Todo', form=form)

# Diese Funktion ermöglicht eine API GET Anfrage aller ToDos
@app.route('/api/todos/all', methods=['GET'])
def api_todos():
    todos = Todo.query.order_by(Todo.timestamp.desc()).all()
    return jsonify([t.serialize() for t in todos])

# Diese Funktion ermöglicht eine API GET Anfrage eines spezifischen ToDo
@app.route('/api/todos/<int:todo_id>', methods=['GET'])
def api_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    return jsonify(todo.serialize())

# Diese Funktion ermöglicht es per API einen ToDo zu schliessen
@app.route('/api/todos/close/<int:todo_id>', methods=['PUT'])   
def api_complete_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    todo.status = 'Closed'
    todo.completed_at = datetime.utcnow()
    db.session.commit()
    return jsonify(todo.serialize())

# Diese Funktion ermöglich es per API einen ToDo wieder zu eröffnen
@app.route('/api/todos/reopen/<int:todo_id>', methods=['PUT'])
def api_incomplete_todo(todo_id):
    todo = Todo.query.get_or_404(todo_id)
    todo.status = 'Open'
    todo.completed_at = None
    db.session.commit()
    return jsonify(todo.serialize())										  

# Diese Funktion ermöglicht eine API GET Anfrage aller abgeschlossenen ToDos
@app.route('/api/todos/completed', methods=['GET'])
def api_completed_todos():
    todos = Todo.query.filter_by(status='Closed').order_by(Todo.timestamp.desc()).all()
    return jsonify([t.serialize() for t in todos])

# Diese Funktion ermöglicht eine API GET Anfrage aller nicht abgeschlossenen ToDos
@app.route('/api/todos/incomplete', methods=['GET'])
def api_incomplete_todos():
    todos = Todo.query.filter_by(status='Open').order_by(Todo.timestamp.desc()).all()
    return jsonify([t.serialize() for t in todos])

# Diese Funktion ermöglich es per API einen ToDo hinzuzufügen
@app.route('/api/todos/add', methods=['POST'])
def api_add_todo():
    if not request.json or not 'title' in request.json or not 'priority' in request.json or not 'description' in request.json or not 'department' in request.json  or not 'due_date' in request.json:
        abort(400)
    user = User.query.filter_by(username="ApiUser").first()
    if user is None:
        user = User(username="ApiUser")
        db.session.add(user)
        db.session.commit()
        due_date = datetime.strptime(request.json['due_date'], '%Y-%m-%d').date()
        todo = Todo(title=request.json['title'],priority=request.json['priority'], description=request.json['description'], department=request.json['department'], due_date=request.json['due_date'], author=user)
        db.session.add(todo)
        db.session.commit()
        return jsonify(todo.serialize()), 201
