#Grundaufbau und Basic-Funktionen 1zu1 übernommen aus Beispiel aus Unterricht: https://github.com/miguelgrinberg/microblog
# Alle applikationsspezifischen Funktionen sind eigenentwicklung mit Inspiration von: https://github.com/miguelgrinberg/microblog/blob/main/app/auth/routes.py, https://flask.palletsprojects.com/en/1.0.x/, https://flask-login.readthedocs.io/en/latest/, https://flask-restful.readthedocs.io/en/latest/, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, IntegerField, SelectField, DateField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo

# Formular wir zur Anmeldung benötigt - Stellt die Felder beim login dar
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Passwort', validators=[DataRequired()])
    submit = SubmitField('Anmelden')

# Formular wir zur Registration benötigt - Stellt die Felder beim registrieren dar
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Passwort', validators=[DataRequired()])
    password2 = PasswordField('Passwort wiederholen', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Registrieren')

# Formular wir zur Erstellung und Bearbeitung eines ToDo dar - Stellt die Felder beim abfüllen dar
class TodoForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    description = TextAreaField('Beschreibung', validators=[DataRequired()])
    priority = SelectField('Wichtigkeit', choices=[('1', '1:Low'), ('2', '2:Medium'), ('3', '3:High'), ('4', '4:Critical')], validators=[DataRequired()])
    department = SelectField('Abteilung', choices=[('IT', 'IT'), ('HR', 'HR'), ('FM', 'FM'), ('GL', 'GL')], validators=[DataRequired()])
    due_date = DateField('Abschliessen bis', format='%Y-%m-%d', validators=[DataRequired()])
    submit = SubmitField('Erfassen')

# Formular wir für Kommentare benötigt - Stellt die Felder beim kommentieren dar
class CommentForm(FlaskForm):
    body = TextAreaField('Kommentar', validators=[DataRequired()])
    submit = SubmitField('Kommentieren')
