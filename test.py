#Grundaufbau und Basic-Funktionen 1zu1 übernommen aus Beispiel aus Unterricht: https://github.com/miguelgrinberg/microblog
# Alle applikationsspezifischen Funktionen sind eigenentwicklung mit Inspiration von: https://github.com/miguelgrinberg/microblog/blob/main/app/auth/routes.py, https://flask.palletsprojects.com/en/1.0.x/, https://flask-login.readthedocs.io/en/latest/, https://flask-restful.readthedocs.io/en/latest/, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker
import unittest
from datetime import datetime, timedelta
from app import app, db
from flask_testing import TestCase
from models import User, Todo, Comment, TrackedTodo
from forms import LoginForm, RegistrationForm, TodoForm, CommentForm

class TestApp(TestCase):

    def create_app(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        app.config['WTF_CSRF_ENABLED'] = False
        return app
        
    def setUp(self):
        db.create_all()
        self.user = User(username='testuser', email='test@example.com')
        self.user.set_password('testpassword')
        db.session.add(self.user)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

# Login testen - als Funktion
    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password,
        ), follow_redirects=True)

# Logout testen
    def logout(self):
        return self.client.get('/logout', follow_redirects=True)


# Login testen auf ToDo Hive
    def test_login(self):
        user = User(username='testuser', email='testuser@example.com')
        user.set_password('testpassword')
        response = self.client.post('/login', data=dict(username='testuser', password='testpassword'), follow_redirects=True)
        self.assertIn(b'ToDo Hive', response.data)

# Registration testen
    def test_registration(self):
        response = self.client.post('/register', data=dict(username='newuser', email='newuser@example.com', password='newpassword', password2='newpassword'), follow_redirects=True)
        self.assertIn(b'Sie sind nun erfolgreich registriert', response.data)

# Test - Neuen ToDo erfassen
    def test_new_todo(self):
        self.login('testuser', 'testpassword')
        due_date = datetime.now() + timedelta(days=1)
        response = self.client.post('/new_todo', data=dict(title='New todo', description='A new todo', priority=1, department='IT', due_date=due_date.strftime('%Y-%m-%d')), follow_redirects=True)
        self.assertIn(b'Der ToDo wurde hinzugef', response.data)

# Test - Neuen Kommentar bei einem ToDo erstellen
    def test_comment(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.post(f'/todo/{todo.id}', data=dict(body='A new comment'), follow_redirects=True)
        self.assertIn(b'Ihr Kommentar wurde hinzugef', response.data)

# Test - ToDo abschliessen
    def test_complete_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get(f'/complete_todo/{todo.id}', follow_redirects=True)
        self.assertIn(b'Der ToDo wurde abgeschlossen', response.data)

# Test - ToDo wiedereröffnen
    def test_reopen_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get(f'/reopen_todo/{todo.id}', follow_redirects=True)
        self.assertIn(b'Der ToDo wurde wieder', response.data)

# Test - ToDo überwachen und nicht mehr überwachen
    def test_track_untrack_todos(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get(f'/track_todo/{todo.id}', follow_redirects=True)
        self.assertIn(b'berwachst nun diesen ToDo', response.data)
        response = self.client.get(f'/track_todo/{todo.id}', follow_redirects=True)
        self.assertIn(b'berwachst nun diesen ToDo nicht mehr', response.data)

# Test - Profil-Seite aufrufen
    def test_profile_page(self):
        self.login('testuser', 'testpassword')
        response = self.client.get('/profile', follow_redirects=True)
        self.assertIn(b'Profil', response.data)

# Test - Meine ToDos anzeigen lassen
    def test_my_open_todos(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test Todo', description='This is a test todo', priority=1, department='Test', due_date=datetime.utcnow(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get('/profile', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Test Todo', response.data)

# Test - ToDo löschen
    def test_delete_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.post(f'/delete_todo/{todo.id}', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Ihr Todo-Eintrag wurde entfernt', response.data)
        deleted_todo = db.session.get(Todo, todo.id)
        self.assertIsNone(deleted_todo)

# Test - ToDo bearbeiten
    def test_edit_todo(self):
        due_date = datetime(2026, 4 ,19)
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test todo', description='A test todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.post(f'/edit_todo/{todo.id}', data=dict(title='Updated Test Todo', description='This is an updated test todo', priority=2, department='HR', due_date=due_date.strftime('%Y-%m-%d')), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Ihr Todo-Eintrag wurde aktualisiert.', response.data)
        updated_todo = db.session.get(Todo, todo.id)
        self.assertEqual(updated_todo.title, 'Updated Test Todo')
        self.assertEqual(updated_todo.description, 'This is an updated test todo')
        self.assertEqual(updated_todo.priority, 2)
        self.assertEqual(updated_todo.department, 'HR')

# Test - Per API ToDos anzeigen lassen
    def test_api_todos_all(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test API todo', description='A test API todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get('/api/todos/all', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertTrue(any(d['title'] == 'Test API todo' for d in data))

# Test - Per API spezifischen ToDo anzeigen lassen   
    def test_api_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test API todo', description='A test API todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.get(f'/api/todos/{todo.id}', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(data['title'], 'Test API todo')

# Test - Per API ToDo schliessen
    def test_api_close_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test API todo', description='A test API todo', priority=1, department='IT', due_date=datetime.now(), author=self.user)
        db.session.add(todo)
        db.session.commit()
        response = self.client.put(f'/api/todos/close/{todo.id}', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(data['status'], 'Closed')

# Test - Per API ToDo wiedereröffnen
    def test_api_reopen_todo(self):
        self.login('testuser', 'testpassword')
        todo = Todo(title='Test API todo', description='A test API todo', priority=1, department='IT', due_date=datetime.now(), author=self.user, status='Closed', completed_at=datetime.now())
        db.session.add(todo)
        db.session.commit()
        response = self.client.put(f'/api/todos/reopen/{todo.id}', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(data['status'], 'Open')

if __name__ == '__main__':
    unittest.main(verbosity=2)
