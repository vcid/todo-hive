# Teil übernommen, Teil Eigentwicklung, Inspiration aus: https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/, https://pythonhosted.org/Flask-Bootstrap/, https://flask-login.readthedocs.io/en/latest/, https://github.com/tiangolo/uwsgi-nginx-flask-docker, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
bootstrap = Bootstrap(app)
login.login_view = 'login'


from routes import *

if __name__ == '__main__':
    app.run()

