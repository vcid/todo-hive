#Grundaufbau und Basic-Funktionen 1zu1 übernommen aus Beispiel aus Unterricht: https://github.com/miguelgrinberg/microblog
# Alle applikationsspezifischen Funktionen sind eigenentwicklung mit Inspiration von: https://github.com/miguelgrinberg/microblog/blob/main/app/auth/routes.py, https://flask.palletsprojects.com/en/1.0.x/, https://flask-login.readthedocs.io/en/latest/, https://flask-restful.readthedocs.io/en/latest/, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker
from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login

# Konfiguriert die Tabelle User mit den entsprechenden Datensätzen und Verbindungen zu anderen Tabellen
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    todos = db.relationship('Todo', backref='author', lazy='dynamic')
    tracked_todos = db.relationship('TrackedTodo', backref='user', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

# Konfiguriert die Tabelle ToDo mit den entsprechenden Datensätzen und Verbindungen zu anderen Tabellen
class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(200), nullable=True)
    priority = db.Column(db.Integer, nullable=False)
    department = db.Column(db.String(255), nullable=False) 
    due_date = db.Column(db.Date, nullable=False)
    status = db.Column(db.String(20), nullable=False, default='Open')
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    completed_at = db.Column(db.DateTime)

    def __repr__(self):
        return f'<Todo {self.title}>'

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'priority': self.priority,
            'department': self.department,
            'due_date': self.due_date,
            'status': self.status,
            'created_at': self.timestamp,
            'completed_at': self.completed_at,
            'author_username': self.author.username,
        }

# Konfiguriert die Tabelle Kommentar mit den entsprechenden Datensätzen und Verbindungen zu anderen Tabellen
class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(200), nullable=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    todo_id = db.Column(db.Integer, db.ForeignKey('todo.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

# Konfiguriert die Tabelle Überwachte ToDo mit den entsprechenden Datensätzen und Verbindungen zu anderen Tabellen
class TrackedTodo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    todo_id = db.Column(db.Integer, db.ForeignKey('todo.id'))
    todo = db.relationship('Todo', backref='tracked_by_users', lazy=True)
