# Teil übernommen, Teil Eigentwicklung, Inspiration aus: https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/, https://pythonhosted.org/Flask-Bootstrap/, https://flask-login.readthedocs.io/en/latest/, https://github.com/tiangolo/uwsgi-nginx-flask-docker, als Vorbereitung im Unterricht mit Jonas besprochen, daher als mögliche Quelle: https://gitlab.com/jonbelbel1/incident_tracker

FROM python:3.8-slim

WORKDIR /app

COPY requirements.txt requirements.txt 
COPY app.py app.py
COPY config.py config.py
COPY templates templates
COPY static static
COPY entrypoint.sh entrypoint.sh
COPY forms.py forms.py
COPY models.py models.py
COPY routes.py routes.py

RUN python -m pip install --upgrade pip

RUN pip install -r requirements.txt

RUN chmod a+x ./entrypoint.sh

Expose 5000

ENTRYPOINT ["./entrypoint.sh"]

CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]
