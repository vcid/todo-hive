# ToDo Hive
![Logo ToDo Hive](https://gitlab.com/vcid/todo-hive/-/raw/main/Img/ToDoHive.png)

ToDo Hive is a Flask-based web application that allows users to manage their todos. It includes features such as user authentication, adding, editing, completing and reopening todos.
Frontend is written in german

You can find the live page under the following link: https://todo-hive.simondurrer.ch

## Getting started

To get started with ToDo Hive, follow these steps:

1. Clone the repository to your local machine. "git clone https://gitlab.com/vcid/todo-hive.git"
2. Create a virtual environment and activate it.
3. Install the required packages with pip install -r requirements.txt.
4. Create a .env file with the following content:
makefile
Copy code
FLASK_APP=run.py
FLASK_ENV=development
SECRET_KEY=your_secret_key
SQLALCHEMY_DATABASE_URI=your_database_uri

5. Replace your_secret_key with a secret key of your choice, and your_database_uri with the URI of your database.
6. Run the application with flask run.

## Files
The source code of ToDo Hive is organized as follows:

app.py: the main Flask application.
models.py: contains the data models used by the application.
routes.py: contains the view functions that handle the different routes of the application.
forms.py: contains the Flask-WTF forms used by the application.
config.py: contains the configuration options used by the application.
run.py: the entry point of the application.
In addition, the following directories are included:

static: contains the static files (CSS, JavaScript, images, etc.) used by the application.
templates: contains the HTML templates used by the application.

## Usage
Once you have the server running, you can start using Todo Hive. Here are some of the things you can do:

Create a new task by clicking the "Add Task" button
View all tasks by clicking the "All Tasks" button
Edit a task by clicking the "Edit" button next to it
Delete a task by clicking the "Delete" button next to it
You can also filter tasks by their status (completed or not completed) using the dropdown menu in the navbar.

## Test

Test procedure will follow


# Credits

ToDo Hive was developed by Simon Durrer. Feel free to use and modify the code for your own projects.
